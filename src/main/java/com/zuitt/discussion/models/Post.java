package com.zuitt.discussion.models;

import javax.persistence.*;

//marks this java object as a representation of an entity/record from the database table "posts".
@Entity
@Table(name = "posts")
public class Post {
    // properties
    //primary key
    @Id
    //auto increment
    @GeneratedValue
    private Long id;


    @Column
    private String title;
    @Column
    private String content;

    @ManyToOne
    @JoinColumn (name = "user_id", nullable = false)
    private User user;
    //constructor
    public Post(){

    }
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
