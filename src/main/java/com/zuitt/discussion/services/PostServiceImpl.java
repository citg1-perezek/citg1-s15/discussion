// Contains the business logic concerned with a particular object in the class.

package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService{
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtToken jwtToken;


    public void createPost(String stringToken, Post post) {
        //Retrieve the "user" object using the extracted username from the token
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);

    }

    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    public ResponseEntity deletePost(Long id, String stringToken){
 /* postRepository.deleteById(id);
  return new ResponseEntity<>("Post deleted Successfully", HttpStatus.OK);*/

        Post postForDeleting = postRepository.findById(id).get();
        String postAuthorName = postForDeleting.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(postAuthorName)){
            return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity updatePost(Long id, String stringToken, Post post){
/* Post postForUpdate = postRepository.findById(id).get();
  postForUpdate.setTitle(post.getTitle());
  postForUpdate.setContent(post.getContent());
  postRepository.save(postForUpdate);
 return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);*/
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthorName = postForUpdating.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(postAuthorName)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }

    }
    public Iterable<Post> getMyPosts(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getPosts();
    }

}


