// Services is an interface that exposes the methods of an implementation whose details have been abstracted away.
package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    // create a post
    void createPost(String stringToken, Post post);

    // viewing all post
    Iterable<Post> getPosts();

    //delete a post
    ResponseEntity deletePost(Long id, String stringToken);

    ResponseEntity updatePost(Long id, String stringToken, Post post);

    //To get all posts of a specific user
    Iterable<Post> getMyPosts(String stringToken);
}



